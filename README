Online Monitor for the Pandas Dataframe Transformations
*******************************************************

     Author: Igor Marfin

     Contact: <igor.marfin@unister.de>

     Organization: private

     Date: Oct 8, 2015, 9:06:51 AM

     Status: draft

     Version: 1

     Copyright: This document has been placed in the public domain. You
     may do with it as you wish. You may copy, modify, redistribute,
     reattribute, sell, buy, rent, lease, destroy, or improve it, quote
     it at length, excerpt, incorporate, collate, fold, staple, or
     mutilate it, or do anything else to it that your or anyone else's
     heart desires.

     Dedication: For statisticians

     Abstract: This study investigates the problem of the portfolio
     optimization. Several practical approaches are addressed to find
     the optimal portfolio of instruments which are used in the trading
     operations.

1 Introduction to the topic and motivation
******************************************

I want to introduce you to my motivation which forces me to develop
this project.  The main part of any statistical inference is
preparation of data to consumable format and generation of new data on
the basis of existing information.  For `pythonic' statisticians, this
means that

  1. a pandas dataframe has to be built from the database or file

  2. new data should be inserted into the dataframe

  3. the dataframe should be transformed (normalized)

  All steps usually take much time if the number of entries is large.

  If you run your analysis on the desktop, you can logout the trace of
your computations to the display via calls of `sys.stdout(...) or
print(...)' .  In the case when your code is running online in a cloud
or on some remote server, such approach will be problematic. Also the
logging of your calculations to files remotely, can not serve your
intentions  if you don't have a `ssh' tunnel to the remote server.

1.1 Installation of the package
===============================

     Important: The project relies on the presence of `Autotools' in
     the system.  Before, proceed further, please, install them: `sudo
     apt-get install autotools-dev'.

Simply, clone the project

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/pandas-dataframe-transformation-monitor.git

and test a configuration of your system that you have all components
installed:

   * python

   * scientific python modules.

  In order to do this, you can just run the default rule of  `Makefile'
via the command in the shell:

    make


2 Documentation
***************

The main parts of documentation are placed  in the `README.rst'.  The
python and shell codes in `bin/' and `lib/' folders are documented as
well.  The docstring are used for this purpose.  The `Pylit' frameworks
and  two scripts: `source_doc/create_documentation.sh' and
`source_doc/shell2rst.py' can be used to extract the docstrings and
create the documentation of the source code as
`source_doc/{bin,lib}/*.rst' files.

  Running the command

    make doc

will make a folder `doc/' with the `README.html' which collects all
`*.rst' files together in the HTML format.

3 Samples
*********

The file `data/FL_insurance_sample.csv' contains the samples used to
create the pandas dataframe.

  The file was download from spatialkey.com
(https://support.spatialkey.com/spatialkey-sample-csv-data/) (1).

4 References
************

     (1) <https://support.spatialkey.com/spatialkey-sample-csv-data/>

5 Documentation of the source code
**********************************

Here will be the documentation of the code from
`source_doc/{bin,lib}/*rst' inserted.



Local Variables:
coding: utf-8
End:
